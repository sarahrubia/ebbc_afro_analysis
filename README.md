Análise bibliométrica e altmétrica do artigo "[ATENÇÃO ON-LINE E DADOS DE CITAÇÃO DE PESQUISAS COM TEMÁTICAS ÉTNICO-RACIAIS: UM COMPARATIVO ENTRE PUBLICAÇÕES SOBRE AFRODESCENDENTES BRASILEIROS E AMERICANOS](https://repositorio.ufba.br/ri/handle/ri/32385)" publicado nos _anais_ do 7º Encontro Brasileiro de Bibliometria e Cientometria, realizado virtualmente pelo Instituto de Ciência da Informação da Universidade Federal da Bahia (UFBA). <br>

Autores:<br>
__Sarah Rúbia de Oliveira Santos__ <br>
Mestranda em Gestão e Organização do Conhecimento <br>
Universidade Federal de Minas Gerais (UFMG)<br>
E-mail: sarahrubia@ufmg.br<br>

__Ronaldo Ferreira de Araújo__<br>
Doutor em Ciência da Informação<br>
Universidade Federal de Alagoas (UFAL)<br>
E-mail: ronaldo.araujo@ichca.ufal.br
